package com.company;

import com.sun.xml.internal.ws.util.StringUtils;
import jdk.nashorn.internal.ir.WhileNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public  static void printIntArray(int[] arrayForPrint) {
        System.out.print("Array contains values: ");
        for (int i = 0; i < arrayForPrint.length; i++) {
            System.out.print(arrayForPrint[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // getting array from input
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readValue;
        int intReadValue = 0;
        while (true) {
            System.out.print("Please enter the count of elements: ");
            try {
                readValue = reader.readLine();
                intReadValue = Integer.parseInt(readValue);
                if (intReadValue < 1) {
                    System.out.print("Count of elements must be positive whole number. ");
                    continue;
                }
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (NumberFormatException e) {
                System.out.print("Count of elements must contains only digits. ");
            }
        }
        int[] array= new int[intReadValue];
        for (int i = 0; i < array.length; i++) {
            while (true) {
                try {
                    System.out.print("Please enter the " + (i + 1) + " elements value: ");
                    readValue = reader.readLine();
                    intReadValue = Integer.parseInt(readValue);
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    System.out.print("Element value must be whole number. ");
                }
            }
            array[i] = intReadValue;
        }
        System.out.println("initial array.");
        printIntArray(array);
        // even numbers
        System.out.print("Even numbers: ");
        for (int i = 0; i < array.length; i++) {
            if ((array[i]%2 == 0) && (array[i] != 0)) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        // odd numbers
        System.out.print("Odd numbers: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i]%2 != 0) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        // divisible by 3 or 19
        System.out.print("Divisible by 3 or 19: ");
        for (int i = 0; i < array.length; i++) {
            if ((array[i]%3 == 0) || (array[i]%19 == 0)) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        // divisible by 5 and 7
        System.out.print("Divisible by 5 and 7: ");
        for (int i = 0; i < array.length; i++) {
            if ((array[i]%5 == 0) && (array[i]%7 == 0)) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        // Three-digit numbers with no identical decimal notation
        System.out.print("Three-digit numbers with no identical decimal notation: ");
        for (int i = 0; i < array.length; i++) {
            if ((Math.abs(array[i]) >= 100) && (Math.abs(array[i]) <= 999) && (array[i]%10 != array[i]/100) &&
                    (array[i]%10 != array[i]/10 - array[i]/100*10) && (array[i]/100 != array[i]/10 - array[i]/100*10)) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
    }
}