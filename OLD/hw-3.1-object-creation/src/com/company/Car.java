package com.company;

public class Car {
    private int countOfDoors = 5;
    private Car() {
    }

    public  Car(int countOfDoors) {
        this.countOfDoors = countOfDoors;
    }

    public void show() {
        System.out.println(countOfDoors);
    }
}
