package com.company;

public class Car2 {
    private int countOfDoors = 5;
    private Car2() {
    }

    public void show() {
        System.out.println(countOfDoors);
    }

    public static Car2 newCar(int countOfDoors) {
        Car2 car2 = new Car2();
        car2.countOfDoors = countOfDoors;
        return car2;
    }
}
