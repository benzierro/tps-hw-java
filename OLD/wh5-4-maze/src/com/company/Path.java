package com.company;

import java.util.ArrayList;
import java.util.List;

public class Path {
    private List<String> path;
    private int x;
    private int y;

    public Path(List<String> pathClone, int x, int y) {
        this.x = x;
        this.y = y;
        path = new ArrayList<>();
        for (String tmpStr:pathClone) {
            path.add(tmpStr);
        }
    }

    public void move(String move, int x, int y) {
        path.add(move);
        this.x = x;
        this.y = y;
    }

    public List<String> getPath() {
        return path;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        StringBuffer tmpStr = new StringBuffer(path.size() + "\n");
        for (String str:path) {
            tmpStr.append(str + " ");
        }
        return tmpStr.toString();
    }
}
