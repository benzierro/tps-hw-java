package com.company;

public class Test {

    public static void main(String[] args) {
        Maze maze = new Maze();
        maze.createMazeGrid();
        maze.printMazeGrid();
        maze.findPath();
    }
}
