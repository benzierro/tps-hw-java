package com.company;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Maze {
    private char[][] mazeGrid;
    private List<Path> mazePathfinding = new ArrayList<>();
    private List<Move> movesBetter = new ArrayList<>();
    int exitX;
    int exitY;

    public void createMazeGrid() {
        List<String> moves = new ArrayList<>();
        try {
            moves = Files.readAllLines(Paths.get("./resources/Player.txt"), StandardCharsets.UTF_8);
            for (String str: moves) {
                movesBetter.add(new Move(str));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> mazeFromFile = null;
        try {
            mazeFromFile = Files.readAllLines(Paths.get("./resources/Maze.txt"), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        } 
        exitX = mazeFromFile.size() - 1;
        exitY = mazeFromFile.get(0).length() - 1;
        mazeGrid = new char[mazeFromFile.size()][mazeFromFile.get(0).length()];
        for (int i = 0; i < mazeFromFile.size(); i++) {
            for (int j = 0; j < mazeFromFile.get(0).length(); j++) {
                mazeGrid[i][j] = mazeFromFile.get(i).toCharArray()[j];
            }
        }
    }

    public void printMazeGrid() {
        for (int i = 0; i < mazeGrid.length; i++) {
            for (int j = 0; j < mazeGrid[i].length; j++) {
                System.out.print(mazeGrid[i][j]);
            }
            System.out.println();
        }
    }

    public void moveNextCell(Path path) {
        for (Move move: movesBetter) {
            int x = path.getX() + move.getCorrX();
            int y = path.getY() + move.getCorrY();
            if ((x>=0) && (x<=exitX) && (y>=0) && (y<=exitY) && mazeGrid[x][y] == 'Y') {
                path.move(move.getMove(), x, y);
                mazeGrid[x][y] = 'N';
                return;
            }
        }
    }

    public int getAvailebleCellCnt(Path path) {
        int cnt = 0;
        for (Move move: movesBetter) {
            int x = path.getX() + move.getCorrX();
            int y = path.getY() + move.getCorrY();
            if ((x>=0) && (x<=exitX) && (y>=0) && (y<=exitY) && mazeGrid[x][y] == 'Y') {
                cnt++;
            }
        }
        return cnt;
    }

    private String pathfinding() {
        mazePathfinding.add(new Path(new ArrayList<>(),0,0));
        mazeGrid[0][0] = 'N';
        boolean fl = true;
        int availebleCellCnt;
        Path curPath;
        while (fl) {
            int cnt = mazePathfinding.size();
            for (int i = 0; i < cnt; i++) {
            availebleCellCnt = getAvailebleCellCnt(mazePathfinding.get(i));
                for (int j = 0; j < availebleCellCnt; j++) {
                    if (j==availebleCellCnt - 1) {
                        curPath = mazePathfinding.get(i);
                    } else {
                        curPath = new Path(mazePathfinding.get(i).getPath(), mazePathfinding.get(i).getX(), mazePathfinding.get(i).getY());
                        mazePathfinding.add(curPath);
                    }
                    moveNextCell(curPath);
                }
            }
            fl = false;
            List<Path> pathForDelete = new ArrayList<>();
            for (Path path: mazePathfinding) {
                if ((path.getX() == exitX) && (path.getY() == exitY)) {
                    return path.toString();
                }
                if (getAvailebleCellCnt(path) != 0) {
                    fl = true;
                } else {
                    pathForDelete.add(path);
                }
            }
            for (Path path : pathForDelete) {
                mazePathfinding.remove(path);
            }
        }
        return "No way out";
    }

    public void findPath() {
        String str = pathfinding();
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("./resources/output.txt")))
        {
            writer.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
