package com.company;

import java.util.Arrays;
import java.util.List;

public class Move {
    private String move;
    private int corrX;
    private int corrY;

    public Move(String move) {
        this.move = move;
        corrX = 0;
        corrY = 0;
        List<String> movesSequence = Arrays.asList(move.split(" "));
        for (String str: movesSequence) {
            if (str.contains("d")) {
                corrX++;
            }
            if (str.contains("u")) {
                corrX--;
            }
            if (str.contains("r")) {
                corrY++;
            }
            if (str.contains("l")) {
                corrY--;
            }
        }
    }

    public String getMove() {
        return move;
    }

    public void setMove(String move) {
        this.move = move;
    }

    public int getCorrX() {
        return corrX;
    }

    public void setCorrX(int corrX) {
        this.corrX = corrX;
    }

    public int getCorrY() {
        return corrY;
    }

    public void setCorrY(int corrY) {
        this.corrY = corrY;
    }

}
