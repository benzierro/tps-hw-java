package com.company;

import com.sun.xml.internal.ws.util.StringUtils;
import jdk.nashorn.internal.ir.WhileNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public  static void printStringArray(String[] arrayForPrint, boolean isWithLength) {
        System.out.print("Array contains values: ");
        for (int i = 0; i < arrayForPrint.length; i++) {
            System.out.print(arrayForPrint[i]);
            if (isWithLength) {
                System.out.print("(" + arrayForPrint[i].length() + ") ");
            }
            else {
                System.out.print(" ");
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // getting array from input
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readValue;
        int intReadValue = 0;
        while (true) {
            System.out.print("Please enter the count of elements: ");
            try {
                readValue = reader.readLine();
                intReadValue = Integer.parseInt(readValue);
                if (intReadValue < 1) {
                    System.out.print("Count of elements must be positive whole number. ");
                    continue;
                }
                break;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.print("Count of elements must contains only digits. ");
            }
        }
        String[] array = new String[intReadValue];
        for (int i = 0; i < array.length; i++) {
            while (true) {
                try {
                    System.out.print("Please enter the " + (i + 1) + " elements value: ");
                    readValue = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                array[i] = readValue;
                break;
            }
        }
        System.out.println("initial array.");
        printStringArray(array, false);
        // strings with min and max length
        int minValue = array[0].length();
        int minElement = 0;
        int maxValue = array[0].length();
        int maxElement = 0;
        String tmpString;
        for (int i = 1; i < array.length; i++) {
            if (minValue > array[i].length()) {
                minValue = array[i].length();
                minElement = i;
            }
            if (maxValue < array[i].length()) {
                maxValue = array[i].length();
                maxElement = i;
            }
        }
        System.out.println("longest sting in array: " + array[maxElement] + ", length = " + maxValue);
        System.out.println("shortest sting in array: " + array[minElement] + ", length = " + minValue);
        System.out.println();
        // asc sort
        for (int i = 0; i < array.length; i++) {
            minValue = array[i].length();
            minElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (minValue > array[j].length()) {
                    minValue = array[j].length();
                    minElement = j;
                }
            }
            tmpString = array[minElement];
            array[minElement] = array[i];
            array[i] = tmpString;
        }
        System.out.println("Array after ascending sort by length.");
        printStringArray(array, true);

        // desc sort
        for (int i = 0; i < array.length; i++) {
            maxValue = array[i].length();
            maxElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (maxValue < array[j].length()) {
                    maxValue = array[j].length();
                    maxElement = j;
                }
            }
            tmpString = array[maxElement];
            array[maxElement] = array[i];
            array[i] = tmpString;
        }
        System.out.println("Array after descending sort by length.");
        printStringArray(array, true);

        // avg search
        float avgLengthValue = 0;
        for (int i = 0; i < array.length; i++) {
            avgLengthValue += array[i].length();
        }
        avgLengthValue = avgLengthValue/array.length;
        System.out.println();
        System.out.print("Shorter length than average: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() < avgLengthValue) {
                System.out.print(array[i] + "(" + array[i].length() + ") ");
            }
        }
        System.out.println();
        System.out.print("Longer length than average: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() > avgLengthValue) {
                System.out.print(array[i] + "(" + array[i].length() + ") ");
            }
        }
        System.out.println();
    }
}