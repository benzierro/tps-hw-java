package com.company;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    public static void unpackFile(String name) {
        Map<String, String> wordsMapForPack = null;
        try {
            FileInputStream fis = new FileInputStream("./resources/.ArchiveKey" + name);
            ObjectInputStream oin = new ObjectInputStream(fis);
            wordsMapForPack = (Map<String, String>) oin.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Map<String, String> wordsMapForUnpack = new HashMap<>();
        wordsMapForPack.entrySet().stream().forEach(e -> wordsMapForUnpack.put(e.getValue(), e.getKey()));

        List<String> linesFromFile = null;
        try {
            linesFromFile = Files.readAllLines(Paths.get("./resources/Archived" + name), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("./resources/Unpacked" + name)))
        {
            for (String tmpStr : linesFromFile) {
                String newStr = tmpStr;
                for (String tmpStr2 : wordsMapForUnpack.keySet()) {
                    newStr = newStr.replaceAll(tmpStr2, wordsMapForUnpack.get(tmpStr2));
                }
                writer.write(newStr + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void packFile(String name) {
        List<String> linesFromFile = null;
        try {
            linesFromFile = Files.readAllLines(Paths.get("./resources/" + name), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> words = new ArrayList<>();
        linesFromFile.forEach(tmpStr ->
                words.addAll(Arrays.asList(tmpStr.replaceAll("[().,;?!«»]", "").replaceAll("[\\s]{2,}", " ").toLowerCase().split(" "))));

        Map<String, Integer> wordsMap = new HashMap<>();
        words.stream().distinct().forEach(tmpStr -> wordsMap.put(tmpStr, Collections.frequency(words, tmpStr)));

        List<String> wordsForPack = new ArrayList<>();
        wordsMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(e -> wordsForPack.add(e.getKey()));

        char i = 'a';
        Map<String, String> wordsMapForPack = new HashMap<>();
        for (String tmpStr : wordsForPack) {
            wordsMapForPack.put(tmpStr, String.valueOf(i++));
        }

        try {
            FileOutputStream fos = new FileOutputStream("./resources/.ArchiveKey" + name);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(wordsMapForPack);
            oos.flush();
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("./resources/Archived" + name)))
        {
            for (String tmpStr : linesFromFile) {
                String newStr = tmpStr.replaceAll("[().,;?!«»]", "").replaceAll("[\\s]{2,}", " ").toLowerCase();
                for (String tmpStr2 : wordsForPack) {
                    newStr = newStr.replaceAll(tmpStr2, wordsMapForPack.get(tmpStr2));
                }
                writer.write(newStr + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        packFile("Test.txt");
        unpackFile("Test.txt");
    }
}
