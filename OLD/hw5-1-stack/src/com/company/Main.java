package com.company;

import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        boolean fl = true;
        String num = null;
        while (fl) {
            System.out.print("Please enter the whole number:");
            num = myObj.nextLine();
            try {
                Integer.parseInt(num);
                fl = false;
            } catch (NumberFormatException e) {
                System.out.print("Input string is not correct. ");
            }
        }

        Stack stack = new Stack<>();
        boolean isMinus = false;
        for (int i = 0; i < num.length(); i++) {
            if (!num.substring(i, i + 1).equals("-")) {
                stack.push(num.substring(i, i + 1));
            } else {
                isMinus = true;
            }
        }
        if (isMinus) {
            stack.push("-");
        }
        System.out.print("Reverse number is ");
        while (!stack.empty()) {
            System.out.print(stack.pop());
        }
    }
}
