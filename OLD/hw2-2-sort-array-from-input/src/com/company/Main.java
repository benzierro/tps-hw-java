package com.company;

import com.sun.xml.internal.ws.util.StringUtils;
import jdk.nashorn.internal.ir.WhileNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public  static void printIntArray(int[] arrayForPrint) {
        System.out.print("Array contains values: ");
        for (int i = 0; i < arrayForPrint.length; i++) {
            System.out.print(arrayForPrint[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readValue;
        int[] array;
        while (true) {
            System.out.print("Please enter the array of numbers separated by spaces: ");
            try {
                readValue = reader.readLine();
                String[] str = readValue.split(" ");
                array = new int[str.length];
                for (int i = 0; i < str.length; i++) {
                        array[i] = Integer.parseInt(str[i]);
                }
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (NumberFormatException e) {
                System.out.print("Input array contains not only numbers. ");
            }
        }
        System.out.println("initial array.");
        printIntArray(array);

        // asc sort
        int minValue;
        int minElement;
        for (int i = 0; i < array.length; i++) {
            minValue = array[i];
            minElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (minValue > array[j]) {
                    minValue = array[j];
                    minElement = j;
                }
            }
            array[minElement] = array[i];
            array[i] = minValue;
        }
        System.out.println("Array after ascending sort.");
        printIntArray(array);

        // desc sort
        int maxValue;
        int maxElement;
        for (int i = 0; i < array.length; i++) {
            maxValue = array[i];
            maxElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (maxValue < array[j]) {
                    maxValue = array[j];
                    maxElement = j;
                }
            }
            array[maxElement] = array[i];
            array[i] = maxValue;
        }
        System.out.println("Array after descending sort.");
        printIntArray(array);
    }
}