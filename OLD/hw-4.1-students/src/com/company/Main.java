package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        StudentsList studentsList = new StudentsList();
        try {
            studentsList.addStudent(new Student("1","1","1",
                    format.parse(new String("01.01.1991")), "1","1","Engineering",1,"011801"));
            studentsList.addStudent(new Student("2","2","2",
                    format.parse(new String("02.02.1992")), "2","2","Engineering",1,"011801"));
            studentsList.addStudent(new Student("3","3","3",
                    format.parse(new String("03.03.1993")), "3","3","Engineering",1,"011801"));
            studentsList.addStudent(new Student("5","5","5",
                    format.parse(new String("05.05.1995")), "5","5","Engineering",1,"011802"));
            studentsList.addStudent(new Student("6","6","6",
                    format.parse(new String("06.06.1996")), "6","6","Engineering",2,"011701"));
            studentsList.addStudent(new Student("7","7","7",
                    format.parse(new String("07.07.1997")), "7","7","Philosophy",1,"021801"));
            studentsList.addStudent(new Student("8","8","8",
                    format.parse(new String("08.08.1998")), "8","8","Philosophy",2,"021701"));
            studentsList.addStudent(new Student("9","9","9",
                    format.parse(new String("09.09.1999")), "9","9","Philosophy",2,"021701"));
            studentsList.addStudent(new Student("4","4","4",
                    format.parse(new String("04.04.1994")), "4","4","Engineering",1,"011801"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(studentsList.toString());
        studentsList.showStudentInFaculty("Engineering");
        studentsList.showStudentByFacultyAndCourse();
        studentsList.showStudentBirthAfterYear(1996);
        studentsList.showStudentInGroup("Philosophy",2,"021701");
    }
}
