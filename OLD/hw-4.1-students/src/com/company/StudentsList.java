package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class StudentsList {
    private Student[] students = new Student[5];
    int currentStudent = 0;

    public void addStudent(Student student) {
        if (currentStudent < students.length){
            students[currentStudent] = student;
        } else {
            Student[] tmpStudents = new Student[students.length + 5];
            for (int i = 0; i < students.length; i++) {
                tmpStudents[i] = students[i];
            }
            students = new Student[students.length + 5];
            for (int i = 0; i < currentStudent; i++) {
                students[i] = tmpStudents[i];
            }
            students[currentStudent] = student;
        }
        currentStudent++;
    }

    public void showStudentInFaculty(String facultyName) {
        String str = "Students in faculty of " + facultyName + ":\n";
        for (int i = 0; i < students.length; i++) {
            if ((students[i] != null) && (students[i].getFaculty().equals(facultyName))) {
                str += students[i].toString() + "\n";
            }
        }
        System.out.println(str);
    }

    private boolean stringArrayContaints(String[] strings, String string) {
        for (int i = 0; i < strings.length; i++) {
            if ((strings[i] != null) && (strings[i].equals(string))) {
                return true;
            }
        }
        return false;
    }

    private String[] getFacultyList() {
        String[] tmpArray = new String[students.length];
        int curElement = 0;
        for (int i = 0; i < students.length; i++) {
                if ((students[i] != null) && (!stringArrayContaints(tmpArray, students[i].getFaculty()))) {
                    tmpArray[curElement] = students[i].getFaculty();
                    curElement += 1;
            }
        }
        String[] result = new String[curElement];
        result = Arrays.copyOf(tmpArray, curElement);
        Arrays.sort(result);
        return result;
    }

    private boolean intArrayContaints(int[] array, int value, int maxElement) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

    private int[] getCourseList(String facultyName) {
        int[] tmpArray = new int[students.length];
        int curElement = 0;
        for (int i = 0; i < students.length; i++) {
            if ((students[i] != null) && (students[i].getFaculty().equals(facultyName))
                    && (!intArrayContaints(tmpArray, students[i].getCourse(), curElement))) {
                tmpArray[curElement] = students[i].getCourse();
                curElement += 1;
            }
        }
        int[] result = new int[curElement];
        result = Arrays.copyOf(tmpArray, curElement);
        Arrays.sort(result);
        return result;
    }

    public void showStudentByFacultyAndCourse() {
        String str = "Student by faculty and course.\n";
        String[] facultyList = getFacultyList();
        for (String facultyName : facultyList) {

            int[] courseList = getCourseList(facultyName);
            for (int i : courseList) {
                str +=  i + " course in faculty of " + facultyName + ":\n";
                for (Student student: students) {
                    if ((student != null) && (student.getFaculty().equals(facultyName)) && (i == student.getCourse())) {
                        str += student.toString() + "\n";
                    }
                }
            }
        }
        System.out.println(str);
    }

    public void showStudentBirthAfterYear(int year) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy");
            String yearStr = Integer.toString(year + 1);
            Date date = format.parse(yearStr);
            String str = "Student brth after year of " + year + ":\n";
            for (Student student: students) {
                if ((student != null) && student.getDateOfBirth().after(date)) {
                    str += student.toString() + "\n";
                }
            }
            System.out.println(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void showStudentInGroup(String facultyName, int course, String groupName) {
        String str = "Students from group " + groupName + " from " + course + " course in faculty of " + facultyName + ":\n";
        for (int i = 0; i < students.length; i++) {
            if ((students[i] != null) && (students[i].getFaculty().equals(facultyName))
            && (students[i].getGroup().equals(groupName)) && (course == students[i].getCourse())) {
                str += students[i].toString() + "\n";
            }
        };
        System.out.println(str);
    }

    @Override
    public String toString() {
        String str = "All students: \n";
        for (int i = 0; i < students.length; i++) {
            if (students[i] != null) {
                str += students[i].toString() + "\n";
            }
        };
        return str;
    }
}
