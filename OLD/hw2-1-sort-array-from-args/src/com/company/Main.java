package com.company;

public class Main {

    public  static void printIntArray(int[] arrayForPrint) {
        System.out.print("Array contains values: ");
        for (int i = 0; i < arrayForPrint.length; i++) {
            System.out.print(arrayForPrint[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        // getting array from args
        int[] array = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                array[i] = Integer.parseInt(args[i]);
            }
            catch (NumberFormatException e) {
                System.out.print("Input array contains not only numbers.");
                return;
            }
        }
        System.out.println("initial array.");
        printIntArray(array);

        // asc sort
        int minValue;
        int minElement;
        for (int i = 0; i < array.length; i++) {
            minValue = array[i];
            minElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (minValue > array[j]) {
                    minValue = array[j];
                    minElement = j;
                }
            }
            array[minElement] = array[i];
            array[i] = minValue;
        }
        System.out.println("Array after ascending sort.");
        printIntArray(array);

        // desc sort
        int maxValue;
        int maxElement;
        for (int i = 0; i < array.length; i++) {
            maxValue = array[i];
            maxElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (maxValue < array[j]) {
                    maxValue = array[j];
                    maxElement = j;
                }
            }
            array[maxElement] = array[i];
            array[i] = maxValue;
        }
        System.out.println("Array after descending sort.");
        printIntArray(array);

    }
}
