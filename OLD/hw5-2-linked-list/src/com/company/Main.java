package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<Integer> al = new ArrayList<>();
        List<Integer> ll = new LinkedList<>();
        Random rand = new Random();

        long start=System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            al.add(i);
        }
        long end=System.currentTimeMillis();
        System.out.println("Insert 1000000 values into ArrayList " + (end-start) + " ms.");

        start=System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            ll.add(i);
        }
        end=System.currentTimeMillis();
        System.out.println("Insert 1000000 values into LinkedList " + (end-start) + " ms.");


        start=System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            al.get(rand.nextInt(1000000));
        }
        end=System.currentTimeMillis();
        System.out.println("Get 1000000 values from ArrayList " + (end-start) + " ms.");

        start=System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            ll.get(rand.nextInt(1000000));
        }
        end=System.currentTimeMillis();
        System.out.println("Get 1000000 values from LinkedList " + (end-start) + " ms.");

    }
}
