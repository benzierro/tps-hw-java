package com.company;

import java.util.Arrays;
import java.util.Objects;

public class PhotoAlbum {
    private Photo[] photos;
    private int currentPhoto;

    public PhotoAlbum(int maxCountOfPhotos) {
        this.photos = new Photo[maxCountOfPhotos];
        currentPhoto = 0;
    }

    public void addPhoto()
    {
        if (currentPhoto < photos.length) {
            photos[currentPhoto] = new Photo();
            currentPhoto += 1;
        }
        else {
            System.out.println("Photo album is full.");
        }
    }

    public void setPhotoName(String name) {
        if (currentPhoto == 0) {
            System.out.println("No photos added.");
        }
        else {
            photos[currentPhoto - 1].setName(name);
        }
    }

    public void printCountOfPhotos() {
        System.out.println("Count of photos in album = " + currentPhoto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhotoAlbum that = (PhotoAlbum) o;
        if (that.hashCode() != hashCode()) {
            return  false;
        }
        return Arrays.equals(photos, that.photos);
    }

    @Override
    public int hashCode() {
        int result = 31 * Arrays.hashCode(photos);
        return result;
    }

    @Override
    public String toString() {
        String str = "PhotoAlbum{photos=";
        for (int i = 0; i < currentPhoto; i++) {
            str += photos[i].toString() + " ";
        }
        str += "}";
        return str;
    }
}
