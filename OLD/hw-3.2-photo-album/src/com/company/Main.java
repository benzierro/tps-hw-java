package com.company;

public class Main {

    public static void main(String[] args) {
        PhotoAlbum photoAlbum = new PhotoAlbum(5);
        photoAlbum.printCountOfPhotos();
        photoAlbum.setPhotoName("first");
        photoAlbum.addPhoto();
        photoAlbum.setPhotoName("first");
        photoAlbum.addPhoto();
        photoAlbum.setPhotoName("second");
        photoAlbum.printCountOfPhotos();
        photoAlbum.addPhoto();
        System.out.println(photoAlbum.toString());
    }
}
