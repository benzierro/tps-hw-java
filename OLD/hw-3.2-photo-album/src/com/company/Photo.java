package com.company;

import java.util.Objects;

public class Photo {
    public Photo() {
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Photo photo = (Photo) o;
        if (photo.hashCode() != hashCode()) {
            return false;
        }
        return Objects.equals(name, photo.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        if (name != null) {
            return "Photo{" +
                    "name='" + name + '\'' +
                    '}';
        }
        else  {
            return "Photo{without name}";
        }
    }
}
