package com.company;

import java.util.Arrays;
import java.util.Objects;

public class Diary {
    private Week[] week;
    private int currentWeek;

    public Diary(int countOfWeeks) {
        week = new Week[countOfWeeks];
        currentWeek = 0;
        week[currentWeek] = new Week();
    }

    public void goToNextWeek() {
        if (currentWeek > week.length) {
            System.out.println("This is last week");
        } else {
            currentWeek++;
            if (week[currentWeek] == null) {
                week[currentWeek] = new Week();
            }
            System.out.print("Current week is " + (currentWeek + 1) + " ");
            System.out.println(week[currentWeek].toString());
        }
    }

    public void goToPreviousWeek() {
        if (currentWeek == 0) {
            System.out.println("This is first week");
        } else {
            currentWeek--;
            System.out.print("Current week is " + (currentWeek + 1) + " ");
            System.out.println(week[currentWeek].toString());
        }
    }

    public void addNoteForDay(Day day, String note) {
        week[currentWeek].addNoteForDay(day, note);
    }

    @Override
    public String toString() {
        String str = "Diary containts notes:\n";
        for (int i = 0; i < week.length; i++) {
            if (week[i] != null) {
                str += "Week " + (i + 1) + " " + week[i].toString() + "\n";
            }
        }
        str += "}";
        return  str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Diary diary = (Diary) o;
        if (diary.hashCode() != hashCode()){
            return false;
        }
        return Arrays.equals(week, diary.week);
    }

    @Override
    public int hashCode() {
        int result = 31 * Arrays.hashCode(week);
        return result;
    }
}