package com.company;

public class Main {

    public static void main(String[] args) {

        Diary diary = new Diary(4);
        diary.addNoteForDay(Day.FRIDAY, "test");
        diary.goToNextWeek();
        diary.addNoteForDay(Day.MONDAY, "test1");
        diary.addNoteForDay(Day.SATURDAY, "test2");
        diary.addNoteForDay(Day.WEDNESDAY, "test3");
        diary.goToPreviousWeek();
        System.out.println(diary.toString());
    }
}