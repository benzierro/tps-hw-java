package com.company;

import java.util.Arrays;

public class Week {
    String[] notes = new String[7];

    public void  addNoteForDay(Day day, String note) {
        notes[day.ordinal()] = note;
    }

    @Override
    public String toString() {
        String str = "{";
        for (Day day : Day.values()) {
            if (notes[day.ordinal()] != null) {
                str +=  day + " = " + "\'" + notes[day.ordinal()] + "\' ";
            }
        }
        str += "}";
        return str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Week week = (Week) o;
        if (week.hashCode() != hashCode()){
            return false;
        }
        return Arrays.equals(notes, week.notes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(notes);
    }
}