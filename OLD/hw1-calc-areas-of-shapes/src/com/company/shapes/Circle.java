package com.company.shapes;

public class Circle extends Shape {
    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    private int radius;

    @Override
    public void showArea() {
        System.out.println("Area of circle = " + area);
    }
    @Override
    public void calcArea() { area = Math.PI * Math.pow(radius, 2); }
}
