package com.company;

import com.company.shapes.Circle;
import com.company.shapes.Shape;
import com.company.shapes.Square;
import com.company.shapes.Triangle;

public class Main {

    public static void main(String[] args) {
//        Circle circle = new Circle();
//
//        circle.showArea();
//
//        Triangle triangle = new Triangle();
//
//        triangle.showArea();
//
//        Square square = new Square();
//
//        square.showArea();

        Shape circle = new Circle(2);
        Shape triangle = new Triangle(2, 4);
        Shape square = new Square(10);


//        circle.showArea();
//        triangle.showArea();
//        square.showArea();
//
        Shape[] shapes = new Shape[3];
        shapes[0] = circle;
        shapes[1] = triangle;
        shapes[2] = square;

        for (int i = 0; i < shapes.length; i++) {
            shapes[i].calcArea();
            shapes[i].showArea();
        }

    }

}
