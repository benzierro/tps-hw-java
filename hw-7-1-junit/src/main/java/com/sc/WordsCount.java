package com.sc;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class WordsCount {

    public List<String> getLinesFromFile (String filename) {
        List<String> result = new ArrayList<>();
        try {
            result = Files.readAllLines(Paths.get("./resources/" + filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<String> getWordsFromLines (List<String> lines) {
        List<String> result = new ArrayList<>();
        lines.forEach(tmpStr ->
                result.addAll(Arrays.asList(tmpStr.replaceAll("[().,:;?!«»]", "").replaceAll("[\\s]{2,}", " ").toLowerCase().split(" "))));
        return result;
    }

    public HashMap<String, Integer> countWordsInList (List<String> words) {
        HashMap<String, Integer> result = new HashMap<>();
        words.stream().distinct().forEach(tmpStr -> result.put(tmpStr, Collections.frequency(words, tmpStr)));
        return  result;
    }

    public HashMap<String, Integer> countWordsInFile (String filename) {
        List<String> lines = getLinesFromFile(filename);
        List<String> words = getWordsFromLines(lines);
        HashMap<String, Integer> result = countWordsInList(words);
        return  result;
    }
}
