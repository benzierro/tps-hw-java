package com.sc;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestWordsCount {

    @Test
    public void testGetLinesFromFile() {
        WordsCount wordsCount = new WordsCount();
        List<String> expectedResult = new ArrayList<>();
        List<String> lines = wordsCount.getLinesFromFile("test.txt");
        expectedResult.add("«qwe» asd: ,klj (qwe);");
        expectedResult.add("klj. klj zzz!?");
        assertEquals(expectedResult, lines);
    }

    @Test
    public void testGetLinesFromFileNegative() {
        WordsCount wordsCount = new WordsCount();
        List<String> expectedResult = new ArrayList<>();
        List<String> lines = wordsCount.getLinesFromFile("test.txt");
        expectedResult.add("asd zxcv");
        expectedResult.add("qwertyuiop");
        assertNotEquals(expectedResult, lines);
    }

    @Test
    public void testGetWordsFromLines() {
        WordsCount wordsCount = new WordsCount();
        List<String> inputLines = new ArrayList<>();
        List<String> expectedResult = new ArrayList<>();
        inputLines.add("«qwe» asd: ,klj (qwe);");
        inputLines.add("klj. klj zzz!?");
        List<String> words = wordsCount.getWordsFromLines(inputLines);
        expectedResult.add("qwe");
        expectedResult.add("asd");
        expectedResult.add("klj");
        expectedResult.add("qwe");
        expectedResult.add("klj");
        expectedResult.add("klj");
        expectedResult.add("zzz");
        assertEquals(expectedResult, words);
    }

    @Test
    public void testGetWordsFromLinesNegative() {
        WordsCount wordsCount = new WordsCount();
        List<String> inputLines = new ArrayList<>();
        List<String> expectedResult = new ArrayList<>();
        inputLines.add("«qwe» asd: ,klj (qwe);");
        inputLines.add("klj. klj zzz!?");
        List<String> words = wordsCount.getWordsFromLines(inputLines);
        expectedResult.add("qwe");
        expectedResult.add("asd");
        expectedResult.add("hhh");
        expectedResult.add("qwe");
        expectedResult.add("jjj");
        expectedResult.add("klj");
        expectedResult.add("zzz");
        assertNotEquals(expectedResult, words);
    }

    @Test
    public void testCountWordsInList() {
        WordsCount wordsCount = new WordsCount();
        List<String> inputWords = new ArrayList<>();
        inputWords.add("qwe");
        inputWords.add("asd");
        inputWords.add("klj");
        inputWords.add("qwe");
        inputWords.add("klj");
        inputWords.add("klj");
        inputWords.add("zzz");
        HashMap<String, Integer> expectedResult = new HashMap<>();
        HashMap<String, Integer> wordsCnt = wordsCount.countWordsInList(inputWords);
        expectedResult.put("qwe", 2);
        expectedResult.put("asd", 1);
        expectedResult.put("klj", 3);
        expectedResult.put("zzz", 1);
        assertEquals(expectedResult, wordsCnt);
    }

    @Test
    public void testCountWordsInListNegative() {
        WordsCount wordsCount = new WordsCount();
        List<String> inputWords = new ArrayList<>();
        inputWords.add("qwe");
        inputWords.add("asd");
        inputWords.add("klj");
        inputWords.add("qwe");
        inputWords.add("klj");
        inputWords.add("klj");
        inputWords.add("zzz");
        HashMap<String, Integer> expectedResult = new HashMap<>();
        HashMap<String, Integer> wordsCnt = wordsCount.countWordsInList(inputWords);
        expectedResult.put("qwe", 2);
        expectedResult.put("asd", 1);
        expectedResult.put("zxc", 1);
        assertNotEquals(expectedResult, wordsCnt);
    }

    @Test
    public void testCountWordsInFile() {
        WordsCount wordsCount = new WordsCount();
        HashMap<String, Integer> wordsCnt = wordsCount.countWordsInFile("test.txt");
        HashMap<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("qwe", 2);
        expectedResult.put("asd", 1);
        expectedResult.put("klj", 3);
        expectedResult.put("zzz", 1);
        assertEquals(expectedResult, wordsCnt);
    }

    @Test
    public void testCountWordsInFileNegative() {
        WordsCount wordsCount = new WordsCount();
        HashMap<String, Integer> wordsCnt = wordsCount.countWordsInFile("test.txt");
        HashMap<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("qwe", 2);
        expectedResult.put("asd", 1);
        expectedResult.put("zxc", 1);
        assertNotEquals(expectedResult, wordsCnt);
    }
}
