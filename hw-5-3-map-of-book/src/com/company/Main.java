package com.company;

import java.util.*;

public class Main {

    public static <Book> Map<Book, Integer> arrayToMap(Book[] ks) {
        Map<Book, Integer> result = new HashMap<>();
        Arrays.asList(ks).stream().filter(tmpBook -> Objects.nonNull(tmpBook)).distinct()
                .forEach(tmpBook -> result.put(tmpBook, Collections.frequency(Arrays.asList(ks), tmpBook)));
        return result;
    }

    public static void main(String[] args) {
        Book[] bookArray = new Book[10];
        bookArray[0] = new Book("author", "title",123);
        bookArray[1] = new Book("author", "title2",987);
        bookArray[2] = new Book("author", "title",123);
        bookArray[3] = new Book("author2", "title",123);
        bookArray[4] = new Book("author", "title2",987);
        bookArray[5] = new Book("author", "title",555);
        bookArray[6] = new Book("author", "title",123);
        Map<Book, Integer> bookMap = arrayToMap(bookArray);
        System.out.println(bookMap.toString());
    }
}
