package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class GameMaster {
    private final int MAX_LEVEL = 10;

    private boolean isPlay = true;
    private Player player;
    Scanner scanner = new Scanner(System.in);
    Random rnd = new Random();

    public void mainMenu() {
        while (isPlay) {
            System.out.println("Выберите дейтвие: 1 - начать новую игру; 2 - загрузить сохрание; 3 - выйти из игры.");
            try {
                switch (scanner.nextLine()) {
                    case "1":
                        System.out.print("Начало игры. ");
                        createHero();
                        startGame();
                        break;
                    case "2":
                        System.out.print("Загрузка соханения. ");
                        loadHero();
                        startGame();
                        break;
                    case "3":
                        isPlay = false;
                        break;
                    default:
                        throw new Throwable("Выбранное действие отсутсвует. ");
                }
            } catch (Throwable e) {
                System.out.print(e.getMessage());
            }
        }
    }

    public void createHero() {
        System.out.println("Ввведите имя героя.");
        String name = scanner.nextLine();
        player = new Player(name);
    }

    private  void  loadHero() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(
                    new FileInputStream("./resources/save.txt"));
            player = (Player)objectInputStream.readObject();
            objectInputStream.close();
        } catch (IOException e) {
            System.out.println("Сохраниение отсутствует. ");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void startGame() {
        while ((player.getStage() <= MAX_LEVEL) && (!player.isDead()) && (isPlay)) {
            goToNextStage();
            if (!player.isDead()) {
                nextLevelMenu();
            }
            if (player.getStage() == MAX_LEVEL + 1) {
                System.out.println("────────▄──────────────▄");
                System.out.println("────────▌▒█───────────▄▀▒▌");
                System.out.println("────────▌▒▒▀▄───────▄▀▒▒▒▐");
                System.out.println("───────▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐");
                System.out.println("─────▄▄▀▒▒▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐");
                System.out.println("───▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀██▀▒▌");
                System.out.println("──▐▒▒▒▄▄▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄▒▒▌");
                System.out.println("──▌▒▒▐▄█▀▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐");
                System.out.println("─▐▒▒▒▒▒▒▒▒▒▒▒▌██▀▒▒▒▒▒▒▒▒▀▄▌");
                System.out.println("─▌▒▀▄██▄▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▒▌");
                System.out.println("_▌▀▐▄█▄█▌▄▒▀▒▒▒▒▒▒░░░░░░▒▒▒▐");
                System.out.println("▐▒▀▐▀▐▀▒▒▄▄▒▄▒▒▒▒▒░░░░░░▒▒▒▒▌");
                System.out.println("▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒░░░░░░▒▒▒▐");
                System.out.println("─▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒▒▒░░░░▒▒▒▒▌");
                System.out.println("─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐");
                System.out.println("──▀▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▌");
                System.out.println("────▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀");
                System.out.println("───▐▀▒▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀");
                System.out.println("──▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▀");
                System.out.println("Игра пройдена. ");
                System.out.println("-----------------------------------------------------------------------------");
                File file = new File("./resources/save.txt");
                file.delete();
            }
        }
    }

    private void goToNextStage() {
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println(player.getStage() + " уровень, информация о герое: " + player.toString());
        int lvlType = rnd.nextInt(4) + 1;
        if (lvlType == 1) {
            System.out.print("Получение артефакта - ");
            int artLvl = rnd.nextInt(3);
            Artifact artifact = Artifact.values()[artLvl];
            player.addArtifact(artifact);
            System.out.println(Artifact.values()[artLvl].toString());
        } else {
            System.out.print("Встреча с монстром. ");
            monsterMenu();
        }
    }

    private void nextLevelMenu() {
        boolean isGoNextStage = false;
        while ((!isGoNextStage) && (isPlay)) {
            System.out.println("Выберите действие: 1 - перейти на следующий уровень; 2 - сохранить; 3 - выйти из игры.");
            try {
                switch (scanner.nextLine()) {
                    case "1":
                        isGoNextStage = true;
                        player.nextStage();
                        break;
                    case "2":
                        saveHero();
                        break;
                    case "3":
                        isPlay = false;
                        break;
                    default:
                        throw new Throwable("Выбранное действие отсутсвует.");
                }
            } catch (Throwable e) {
                System.out.print(e.getMessage());
            }
        }
    }

    public void saveHero() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream("./resources/save.txt"));
            objectOutputStream.writeObject(player);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void monsterMenu() {
        int monsterLvl = Math.max(1, player.getPower() - 3 + rnd.nextInt(4));
        System.out.println(Monster.values()[monsterLvl].toString());
        boolean isCorrect = false;
        while (!isCorrect) {
            System.out.println("Выберите действие: 1 - начать битву; 2 - использовать артефакт. Ваша сила: " + player.getPower() + ".");
            try {
                switch (scanner.nextLine()) {
                    case "1":
                        fight(Monster.values()[monsterLvl].power);
                        isCorrect = true;
                        break;
                    case "2":
                        if (player.getArtifacts().size() != 0) {
                            artifactMenu();
                        } else {
                            System.out.println("У героя нет артефактов.");
                        }
                        break;
                    default:
                        throw new Throwable("Выбранное действие отсутсвует.");
                }
            } catch (Throwable e) {
                System.out.print(e.getMessage());
            }
        }
    }

    private void fight(int monsterPower) {
        if (monsterPower <= player.getPower()) {
            player.lvlUp();
            System.out.print("Герой победил в битве. Уровень героя увеличен. ");
        } else {
            player.setDead(true);
            System.out.print("Герой умер. ");
        }
    }

    private void artifactMenu() {
        boolean isCorrect = false;
        ArrayList<Artifact> artifacts = new ArrayList<>();
        player.getArtifacts().stream().distinct().forEach(artifact -> artifacts.add(artifact));
        while (!isCorrect) {
            System.out.print("Выберите артефакт:");
            for (int i = 0; i < artifacts.size(); i++) {
                System.out.print(" " + (i + 1) + " - " + artifacts.get(i).toString());
            }
            System.out.println(".");
            try {
                int i = Integer.parseInt(scanner.nextLine());
                if ((i > 0) && i < (artifacts.size() + 1)) {
                    isCorrect = true;
                    player.useArtifact(artifacts.get(i - 1));
                } else {
                    throw new Throwable("Выбранное действие отсутсвует. ");
                }
            } catch (Throwable e) {
                System.out.print(e.getMessage());
            }
        }
    }


}
