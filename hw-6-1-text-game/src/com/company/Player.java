package com.company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Player implements Serializable {
    private String name;                                  // имя героя
    private int stage = 1;                                // уровень игры
    private int level;                                    //уровень героя
    private List<Artifact> artifacts = new ArrayList<>(); // инвентарь героя
    private int bonusPower = 0;                           // бонус силы для текущей битвы, полученный от артефактов
    private boolean isDead = false;                       //

    public Player(String name) {
        this.name = name;
        Random rnd = new Random();
        level = rnd.nextInt(3) + 1;
    }

    public void nextStage() {
        stage++;
        bonusPower = 0;
    }

    public int getStage() {
        return stage;
    }

    public void lvlUp() {
        level++;
    }

    public void addArtifact(Artifact artifact) {
        artifacts.add(artifact);
    }

    public void useArtifact(Artifact artifact) {
        artifacts.remove(artifact);
        bonusPower += artifact.power;
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    public int getPower() {
        return level + bonusPower;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    @Override
    public String toString() {
        return "имя='" + name + '\'' +
                ", уровень=" + level +
                ", список артефактов=" + artifacts +
                '.';
    }
}
