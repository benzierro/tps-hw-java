package com.company;

public enum Artifact {

    POTION("Зелье силы",1),
    SCROLL("Свиток мощи", 2),
    ELIXIR("Элексир несокрушимости", 3);

    String name;
    int power;

    Artifact(String name, int power){
        this.name = name;
        this.power = power;
    }

    @Override
    public String toString() {
        return name + " (сила +" + power + ")";
    }
}
