package com.company;

public enum Monster {

    MURLOC("Мурлок",1),
    SPIDER("Паук",2),
    COBOLD("Кобольд",3),
    SATYR("Сатир",4),
    GNOLL("Гноль",5),
    HARPY("Гарпия",6),
    DWARF("Гном",7),
    SKELETON("Скелет",8),
    TROLL("Троль",9),
    WILDKIN("Совух",10),
    FURBOLG("Фурболг",11),
    GOLEM("Голем",12),
    DRAGON("Дракон",13);

    String name;
    int power;

    Monster(String name, int power){
        this.name = name;
        this.power = power;
    }

    @Override
    public String toString() {
        return "Тип монстра - " + name + ", сила: " + power + ".";
    }
}
